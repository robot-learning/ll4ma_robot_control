#!/usr/bin/python

import rospy
import sys
import copy
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from ll4ma_robot_control.srv import TaskPosition
from std_srvs.srv import Trigger
from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import JointState
#from PlayBackTraj import ReadTraj, execute_traj
from rospkg import RosPack
rp=RosPack()
rp.list()
path=rp.get_path('ll4ma_planner')+'/scripts'
sys.path.insert(0,path)
path=rp.get_path('ll4ma_opt_utils')+'/scripts'
sys.path.insert(0,path)
from plan_client import *
from robot_class import robotInterface
from ll4ma_opt_utils.srv import UpdateWorld
import tf
import tf2_ros
import tf2_geometry_msgs


def broadcast_ee_pose(GraspPose, ref_frame='lbr4_base_link'):
    br = tf.TransformBroadcaster()
    for i in range(10):
        positiontuple = (GraspPose.position.x, GraspPose.position.y, GraspPose.position.z)
        quattuple = (GraspPose.orientation.x, \
                     GraspPose.orientation.y, \
                     GraspPose.orientation.z, \
                     GraspPose.orientation.w)
        br.sendTransform(positiontuple,quattuple,rospy.Time.now(), \
                         "PlannedEEPose", ref_frame)
        rospy.sleep(0.1)

def safeTaskCall(cmd, Safety=True):
    srv_call = rospy.ServiceProxy("/lbr4/task_position_service", TaskPosition)
    uip='n'
    if(Safety):
        uip = raw_input("Execute Task Command?(y/n)")
    else:
        uip = 'y'
    if(uip == 'y'):
        srv_call(cmd)
        
def sendTraj(ri, Traj, Safety=True):
    uip='n'
    if(Safety):
        uip = raw_input("Execute?(y/n)")
    else:
        uip = 'y'
    if(uip == 'y'):
        ri.send_jtraj(Traj)

def getPlanforPose(GoalPose, g_plan):
    while(not g_plan.got_state):
        rate.sleep()
    robot_js=JointState()
    robot_js.name=['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
    j_ids = [i for i in range(len(g_plan.joint_state.name)) if g_plan.joint_state.name[i] in robot_js.name]
    robot_js.position=[g_plan.joint_state.position[idx] for idx in j_ids]
    hand_js=JointState()
    h_ids = [j for j in range(len(g_plan.joint_state.name)) if j not in j_ids]
    hand_js.name=[g_plan.joint_state.name[idx] for idx in h_ids]
    hand_js.position=[g_plan.joint_state.position[idx] for idx in h_ids]
    hand_preshape=copy.deepcopy(hand_js)
    robot_traj, palm_pose=g_plan.get_palm_traj(robot_js, \
                                hand_js,hand_preshape,GoalPose,JointTrajectory(),GoalPose,T=3)
    robot_traj = ri.get_smooth_traj(robot_traj)
    g_plan.viz_traj(robot_traj,t_name='ll4ma_planner/traj')
    return robot_traj

def TransformPose(inpose,target_frame='lbr4_base_link',source_frame='world'):
    tf_b = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tf_b)
    inpose_stamped = tf2_geometry_msgs.PoseStamped()
    inpose_stamped.pose = inpose
    inpose_stamped.header.frame_id = source_frame
    inpose_stamped.header.stamp = rospy.Time(0)

    opose = tf_b.transform(inpose_stamped,target_frame,rospy.Duration(5))
    return opose.pose

if __name__ == '__main__':
    rospy.init_node('Task_VS_Planner_Test')
    g_plan=ll4maPlanner('/lbr4_allegro')
    ri=robotInterface(init_node=False)
    rate=rospy.Rate(10)

    # Service call to the task_position_service
    # Refer ll4ma_robot_control/srv/TaskPosition.srv
    rospy.wait_for_service("/lbr4/task_position_service")
    srv_call = rospy.ServiceProxy("/lbr4/task_position_service", TaskPosition)

    # Service call to stop the task control
    # This would bring the robot to stop in 0.1s (Would be updated to specify stop time in future)
    rospy.wait_for_service("/lbr4/stop_task_position_service")
    stop_call = rospy.ServiceProxy("/lbr4/stop_task_position_service", Trigger)


    # Below lines of code configures ll4ma_planner to move robot to a start pose for the demo.
    # You can use other planners like moveIt for your purpose.
    EnvPoses = []
    SomePose = Pose()
    SomePose.position.x = 0
    SomePose.position.y = 0
    SomePose.position.z = -5
    EnvPoses.append(SomePose)
    EnvPoses.append(SomePose)
    rospy.wait_for_service('/ll4ma_planner/update_world')
    UpdatePlannerEnv = rospy.ServiceProxy('/ll4ma_planner/update_world', UpdateWorld)
    
    (r,p,y) = (1.57, 0.0, 1.57)
    QuatArray = quaternion_from_euler(r,p,y)

    CurrPose = Pose()
    CurrPose.position.x = -0.1345
    CurrPose.position.y = -0.662
    CurrPose.position.z = 0.827678
    CurrPose.orientation.x = 0.6003708848771757 #QuatArray[0]
    CurrPose.orientation.y = -0.02907154403633946 #QuatArray[1]
    CurrPose.orientation.z = 0.012599931506217582 #QuatArray[2]
    CurrPose.orientation.w = 0.7990937915196055 #QuatArray[3]
    #CurrPose = TransformPose(CurrPose)
    broadcast_ee_pose(CurrPose, 'world')
    Traj = getPlanforPose(CurrPose, g_plan)
    sendTraj(ri, Traj)

    # Define the goal pose for the task space control.
    GoalPose = copy.deepcopy(CurrPose)
    GoalPose.position.x = -0.1345
    GoalPose.position.y = -0.662
    GoalPose.position.z = 1.227678
    GoalPose.orientation.x = 0.6003708848771757 #QuatArray[0]
    GoalPose.orientation.y = -0.02907154403633946 #QuatArray[1]
    GoalPose.orientation.z = 0.012599931506217582 #QuatArray[2]
    GoalPose.orientation.w = 0.7990937915196055 #QuatArray[3]
    GoalPose = TransformPose(GoalPose)
    #GoalPose.orientation.x = QuatArray[0]
    #GoalPose.orientation.y = QuatArray[1]
    #GoalPose.orientation.z = QuatArray[2]
    #GoalPose.orientation.w = QuatArray[3]
    broadcast_ee_pose(GoalPose)

    cmd = PoseStamped()
    cmd.header.frame_id = "lbr4_base_link"
    cmd.pose = GoalPose
    # The service call takes posestamped as input. Returns boolean
    safeTaskCall(cmd)
    exit(0)
