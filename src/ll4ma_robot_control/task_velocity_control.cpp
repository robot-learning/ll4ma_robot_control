#include "ll4ma_robot_control/task_velocity_control.h"
#include <lbr4_robot_interface/lbr4_interface.h>

#define POSE_3D_DOF 6

bool TaskVelocityController::configureHook()
{
  log("Initializing Task Velocity Controller...");

  if (!Controller::configureHook())
    return false;

  // Initialize Eigen
  q_.setZero(num_jnts_);
  q_prime_.setZero(num_jnts_);
  q_dot_.setZero(num_jnts_);
  q_dot_dot_.setZero(num_jnts_);
  //q_des_.setZero(num_jnts_);
  q_dot_des_.setZero(num_jnts_);
  q_rest_.setZero(num_jnts_);
  q_dot_posture_.setZero(num_jnts_);
  max_jnt_vel_.setZero(num_jnts_);
  max_jnt_acc_.setZero(num_jnts_);
  x_dot_.setZero(POSE_3D_DOF);
  x_dot_dot_.setZero(POSE_3D_DOF);
  x_dot_des_.setZero(POSE_3D_DOF);
  I_N_.setIdentity(num_jnts_, num_jnts_);
  robot_interface_->kdl_->getJointLimits(0,ub,lb);

  tf_ = boost::shared_ptr<tf::TransformListener>(new tf::TransformListener());

  // ROS
  q_dot_des_ros_.position = std::vector<double>(num_jnts_);
  q_dot_des_ros_.velocity = std::vector<double>(num_jnts_);
  q_dot_des_ros_.effort = std::vector<double>(num_jnts_);

  // Get root name from the robot interface
  base_link_ = robot_interface_->getRootNames()[robot_chain_idx_];

  // Get params from ROS parameter server
  nh_.param("pseudoinverse_tolerance", pseudoinverse_tolerance_, 1e-5);
  nh_.param("dq_finite_diff", dq_, 1e-2);
  nh_.param("use_redundancy_resolution", use_redundancy_resolution_, true);

  //Maximum Joints Velocity from KUKA LWR Manual
  //max_jnt_vel_ << 1.91986, 1.91986, 2.23402, 2.23402, 3.56047, 3.21141, 3.21141;

  max_jnt_vel_ << 1.57, 1.57, 1.57, 1.57, 1.57, 1.57, 1.57;

  max_jnt_acc_ << 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8;
  
  q_rest_ << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;

  //max_jnt_vel_ *= 0.;

  //max_jnt_vel_ << 1.91986, 1.91986, 1.91986, 1.91986, 1.91986, 1.91986, 1.91986;
  
  //nh_.param("max_joint_velocity", max_jnt_vel, 1.5);

  bool success = true;
  success &= nh_.getParam("task_des_topic", task_des_topic_);
  success &= nh_.getParam("jnt_des_topic", joint_des_topic_);

  if (!success)
  {
    log("Could not load topic parameters from param server.", ERROR);
    return false;
  }

  // Initialize ROS
  task_des_sub_ = nh_.subscribe(task_des_topic_, 1, &TaskVelocityController::taskDesCallback, this);
  reload_srv_ = nh_.advertiseService("/task_velocity_control/reload_parameters",
                                     &TaskVelocityController::reloadControlParameters, this);
  joint_des_pub_ = nh_.advertise<sensor_msgs::JointState>(joint_des_topic_,100);

  log("Initialization complete.");
  return true;
}


bool TaskVelocityController::startHook()
{
  log("Waiting for robot state...");
  while (nh_.ok() && !robot_state_received_)
  {
    ros::spinOnce();
    rate_.sleep();
  }
  log("Robot state received!");

  prev_time_ = ros::Time::now();
  log("Controller is running...");
  while (nh_.ok())
  {
    //updateHook();
    ros::spinOnce();
    rate_.sleep();
  }

  log("Control loop exited.");
  return true;
}


void TaskVelocityController::updateHook()
{
  cur_time_ = ros::Time::now();
  dt_ = cur_time_ - prev_time_;
  
  // Get Jacobian and take pseudoinverse (J+)
  robot_interface_->kdl_->getJacobian(robot_chain_idx_, q_, J_);
  getPseudoInverse(J_, J_cross_, pseudoinverse_tolerance_);

  x_dot_ = J_*q_dot_;
  //x_dot_dot_ = (x_dot_des_ - x_dot_);//(dt_.toSec()+1e-8);
  
  // Compute desired control
  q_dot_des_ = J_cross_ * x_dot_des_;
			   
  if (use_redundancy_resolution_)
  {
    // TODO: Select desired resolution method easily here
    Eigen::VectorXd q_null;
    computeManipulabilityRedundancyResolution(q_dot_posture_, J_);
    for(int i = 0; i < num_jnts_; ++i)
      {
	q_null.setZero(num_jnts_);
	q_null[i] = q_dot_posture_[i];
	nullSpaceProjection(q_dot_des_, q_null, J_, J_cross_);
      }
    //computeJointLimitRedundancyResolution(q_dot_posture_, J_);
    //nullSpaceProjection(q_dot_des_, q_dot_posture_, J_, J_cross_);
  }
  
  // Convert to ROS type and publish q_des over topic

  computeJointVelocityLimitSaturation(q_dot_des_);

  x_dot_dot_ = J_*q_dot_des_ - x_dot_;
  /*if (abs(x_dot_dot_.maxCoeff()) > 0.2)
  {
    std::stringstream ss;
    ss << "Acceleration:" << x_dot_dot_;
    ss << "Velocity:" << x_dot_des_;
    ROS_INFO(ss.str().c_str());
    }*/

  robot_interface_->kdl_->getJacobianDot(robot_chain_idx_, q_, q_dot_, J_dot_);
  q_dot_dot_ = J_cross_*(x_dot_dot_ - J_dot_ * q_dot_des_);
  computeJointAccelerationSaturation(q_dot_dot_);
  std::stringstream ss;
  //ss << "CMDS:\n " << x_dot_des_;
  //ROS_INFO(ss.str().c_str());

  //q_dot_des_ =  q_dot_ + q_dot_dot_;//*std::min(dt_.toSec(),1.0/500.0);
  
  for (int i =0; i < num_jnts_; ++i)
  {
    // update desired position to match current position + delta_t_*q_dot
    double delta_q = std::min(dt_.toSec(),1.0/500.0)*q_dot_des_[i];
    q_dot_des_ros_.position[i] = q_[i] + delta_q;
    q_dot_des_ros_.velocity[i] = q_dot_des_[i];
    q_dot_des_ros_.effort[i] = q_dot_dot_[i];
  }

  //ss << "Q_POS:\n " << q_;
  //ROS_INFO(ss.str().c_str());
  
  // Set appropriate header info for ros msg
  q_dot_des_ros_.header.stamp = ros::Time::now();
  q_dot_des_ros_.header.frame_id = base_link_;

  if(robot_state_received_)
    {
      //log("States Received", INFO);
      joint_des_pub_.publish(q_dot_des_ros_);
    }
  else
    log("Joint States Not Received Commands Canceled", ERROR);

  prev_time_ = cur_time_;
}


void TaskVelocityController::stopHook()
{
  // nothing to do here for now
}


void TaskVelocityController::cleanupHook()
{
  // nothing to do here for now
}


void TaskVelocityController::taskDesCallback(geometry_msgs::TwistStamped cmd)
{
  //log("Updating x_dot_desired " + std::to_string(cmd.twist.linear.z));
  // Convert into the root frame prior to setting x_dot_des_
  tf::Vector3 twist_rot(cmd.twist.angular.x,
			cmd.twist.angular.y,
			cmd.twist.angular.z);
  tf::Vector3 twist_vel(cmd.twist.linear.x,
			cmd.twist.linear.y,
			cmd.twist.linear.z);
  if (cmd.header.frame_id != base_link_)
  {
    // NOTE: Not realtime safe
    //tf::Vector3 twist_rot(cmd.twist.angular.x,
    //                      cmd.twist.angular.y,
    //                      cmd.twist.angular.z);
    //tf::Vector3 twist_vel(cmd.twist.linear.x,
    //                      cmd.twist.linear.y,
    //                      cmd.twist.linear.z);
    // log("Converting command velocity from frame " + cmd.header.frame_id + " to " + base_link_);
    try{
      tf_->lookupTransform(base_link_, cmd.header.frame_id, cmd.header.stamp, transform_);
      out_rot_ = transform_.getBasis()*twist_rot;
      out_vel_ = transform_.getBasis()*twist_vel;
    } catch (const std::exception& e)
    {
      for (int i = 0; i < 3; ++i)
      {
        out_vel_[i] = 0.0;
        out_rot_[i] = 0.0;
      }
      log(e.what(), ERROR);
    }
  }
  else
    {
      out_rot_ = twist_rot;
      out_vel_ = twist_vel;
    }
  x_dot_des_[0] = out_vel_[0];
  x_dot_des_[1] = out_vel_[1];
  x_dot_des_[2] = out_vel_[2];
  x_dot_des_[3] = out_rot_[0];
  x_dot_des_[4] = out_rot_[1];
  x_dot_des_[5] = out_rot_[2];
  cur_time_ = cmd.header.stamp;
  updateHook();
}


bool TaskVelocityController::reloadControlParameters(std_srvs::Empty::Request &req,
                                                     std_srvs::Empty::Response &resp)
{
  log("Reloading controller parameters...");

  bool success = true;
  // success &= getGainsFromParamServer("p_gains", nh_, Kp_);
  // success &= getGainsFromParamServer("d_gains", nh_, Kd_);
  if (success)
  {
    log("Successfully reset control parameters.");
    return true;
  }
  else
  {
    log("Failed to load parameters from param server.", WARN);
    return false;
  }
}

void TaskVelocityController::computeManipulabilityRedundancyResolution(Eigen::VectorXd &q_dot_posture_,
                                                                       Eigen::MatrixXd &J_)
{
  JJ_trans_ = J_*J_.transpose();
  double U = computeManipulabilityScore(JJ_trans_);

  // Update q_prime to current q_
  for (int i = 0; i < num_jnts_; ++i)
  {
    q_prime_[i] = q_[i];
  }

  for (int i = 0; i < num_jnts_; ++i)
  {
    // Adjust q_[i] by a small ammount to estimate the gradient
    q_prime_[i] = q_[i] + dq_;

    // Get Jacobian at q_prime_ and compute associated manipulability score
    robot_interface_->kdl_->getJacobian(robot_chain_idx_, q_prime_, J_prime_);
    JJ_trans_ = J_prime_*J_prime_.transpose();
    double U_prime = computeManipulabilityScore(JJ_trans_);
    // Compute gradient estimate
    q_dot_posture_[i] = (U_prime-U)/dq_;
    // Reset q_prime_ for next joint
    q_prime_[i] = q_[i];
  }
}


double TaskVelocityController::computeManipulabilityScore(Eigen::MatrixXd& JJ_trans_)
{
  return sqrt(JJ_trans_.determinant());
}

void TaskVelocityController::computeJointSaturation(Eigen::VectorXd &q_des_, Eigen::VectorXd q_max_)
{
  double alpha = 1;
  //Get the Joint Index That is Maximum Over its Limit
  Eigen::VectorXd abscmd = q_des_.cwiseAbs() - q_max_;
  double max_cmd = abscmd.maxCoeff();
  if (max_cmd > 0)
    {
      for(int i = 0; i < num_jnts_; i++)
	if (abscmd[i] == max_cmd)
	  {
	    //Get the Scale Factor for all Joint Velocities
	    alpha = q_max_[i]/(max_cmd + q_max_[i]);
	    break;
	  }
      q_des_ = q_des_*alpha;
    }
}

void TaskVelocityController::computeJointAccelerationSaturation(Eigen::VectorXd &q_acc_des_)
{
  computeJointSaturation(q_acc_des_, max_jnt_acc_);
}

void TaskVelocityController::computeJointVelocityLimitSaturation(Eigen::VectorXd &q_dot_des_)
{
  computeJointSaturation(q_dot_des_, max_jnt_vel_);
}


void TaskVelocityController::computeJointLimitRedundancyResolution(Eigen::VectorXd &q_dot_posture_,
                                                                   Eigen::MatrixXd &J_)
{
  logSS.str("");
  logSS.clear();
  for(int i = 0; i< num_jnts_; i++)
    {
      q_dot_posture_[i] = (q_rest_[i] - q_[i]);
      if(q_[i] < lb[i] || q_[i] > ub[i])
	{
	  logSS << "Joint " << i+1 << " Over Limit\n";
	  log(logSS.str().c_str(), WARN);
	}
    }
}


void TaskVelocityController::log(std::string msg)
{
  log(msg, INFO);
}


void TaskVelocityController::log(std::string msg, LogLevel level)
{
  switch(level)
  {
    case WARN :
    {
      ROS_WARN_STREAM("[TaskVelocityController] " << msg);
      break;
    }
    case ERROR :
    {
      ROS_ERROR_STREAM("[TaskVelocityController] " << msg);
      break;
    }
    default:
    {
      ROS_INFO_STREAM("[TaskVelocityController] " << msg);
      break;
    }
  }
}
