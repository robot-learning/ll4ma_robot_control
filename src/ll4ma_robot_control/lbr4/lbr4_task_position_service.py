#!/usr/bin/python

import rospy
import numpy as np
import copy
import PyKDL
from geometry_msgs.msg import TwistStamped, PoseStamped
from tf.transformations import euler_from_quaternion, quaternion_multiply
from ll4ma_robot_control.srv import TaskPosition, TaskPositionResponse
from std_srvs.srv import Trigger, TriggerResponse
import tf
import tf2_ros
import tf2_geometry_msgs
from math import exp

class TaskModeHandle:

    def __init__(self):
        self.tip_link = rospy.get_param('tip_links')
        self.base_link = rospy.get_param('root_links')
        self.pub = rospy.Publisher('/lbr4/task_cmd', TwistStamped, queue_size=1)
        self.ctrl_rate = 500 #500Hz
        self.rate = rospy.Rate(self.ctrl_rate)
        self.tf_b = tf2_ros.Buffer()
        self.listener = tf2_ros.TransformListener(self.tf_b)
        self.Kp = 1
        self.max_lin_vel = 0.60
        self.max_rot_vel = 1.57
        self.min_lin_vel = 0.20
        self.min_rot_vel = 0.60
        self.LWt = 45 #Linear Velocity Gain for Scaling
        self.RWt = 45 #Rotary Velocity Gain for Scaling
        self.StopTriggered = False

    def sigmoid(self, x, W, b):
        epx = exp(x*W)
        emx = exp(-x*W)
        ret = (epx - emx) / (epx + emx)
        return x + ret*b

    def saturateTwist(self,in_twist):
        in_twist.vel[0] = self.sigmoid(in_twist.vel[0],self.LWt, self.min_lin_vel)
        in_twist.vel[1] = self.sigmoid(in_twist.vel[1],self.LWt, self.min_lin_vel)
        in_twist.vel[2] = self.sigmoid(in_twist.vel[2],self.LWt, self.min_lin_vel)
        in_twist.rot[0] = self.sigmoid(in_twist.rot[0],self.RWt, self.min_rot_vel)
        in_twist.rot[1] = self.sigmoid(in_twist.rot[1],self.RWt, self.min_rot_vel)
        in_twist.rot[2] = self.sigmoid(in_twist.rot[2],self.RWt, self.min_rot_vel)
        max_lin = max(abs(in_twist.vel.x()),abs(in_twist.vel.y()),abs(in_twist.vel.z()))
        lin_alpha = 1
        rot_alpha = 1
        if max_lin > self.max_lin_vel:
            lin_alpha = self.max_lin_vel/max_lin
            #rospy.loginfo('linear saturation')
        in_twist.vel *= lin_alpha
        in_twist.rot *= lin_alpha
        max_rot = max(abs(in_twist.rot.x()),abs(in_twist.rot.y()),abs(in_twist.rot.z()))
        if max_rot > self.max_rot_vel:
            rot_alpha = self.max_rot_vel/max_rot
            #rospy.loginfo('rotary saturation')
        in_twist.vel *= rot_alpha
        in_twist.rot *= rot_alpha
        return in_twist

    def TriggerStopServer(self, req):
        rospy.loginfo('Stop Triggered for Task Position Call')
        self.StopTriggered = True
        return TriggerResponse(success=True)

    def PositionModeServer(self, req):
        rospy.loginfo('Task Position Service Invoked')
        self.StopTriggered = False
        poscmd = req.cmd_pose
        
        tcmd = TwistStamped()
        tcmd.header.frame_id = poscmd.header.frame_id
        od = np.array([poscmd.pose.orientation.x, \
                              poscmd.pose.orientation.y, \
                              poscmd.pose.orientation.z, \
                              poscmd.pose.orientation.w])
        (rd,pd,yd) = euler_from_quaternion(od)
        xd = np.array([poscmd.pose.position.x, \
                       poscmd.pose.position.y, \
                       poscmd.pose.position.z, \
                       rd,pd,yd])
        trd = PyKDL.Vector(xd[0],xd[1],xd[2])
        rrd = PyKDL.Rotation.Quaternion(od[0],od[1],od[2],od[3])
        fd = PyKDL.Frame(rrd, trd)

        Trans = self.tf_b.lookup_transform(tcmd.header.frame_id,self.tip_link[0],rospy.Time(0),rospy.Duration(5.0))
        oc = np.array([Trans.transform.rotation.x,Trans.transform.rotation.y,Trans.transform.rotation.z,Trans.transform.rotation.w])
        Linear = Trans.transform.translation
        xc = np.array([Linear.x,Linear.y,Linear.z])
        trc = PyKDL.Vector(xc[0],xc[1],xc[2])
        rrc = PyKDL.Rotation.Quaternion(oc[0], oc[1], oc[2], oc[3])
        fc = PyKDL.Frame(rrc, trc)
        KDLtwist = PyKDL.diff(fc,fd,1.0)
        dist = max(abs(KDLtwist.vel.x()),abs(KDLtwist.vel.y()),abs(KDLtwist.vel.z()))

        rospy.loginfo("Initial error Distance: %f", dist)
        
        max_time = (2*dist/self.min_lin_vel) + 2.0
        max_cycles = int(max_time * self.ctrl_rate)

        rospy.loginfo("Setting Max Cycles: %f for Max Time: %d", max_cycles, max_time)

        Kp = self.Kp

        (Slx,Sly,Slz) = (0,0,0)
        (Sax,Say,Saz) = (0,0,0)

        dt = 1.0

        ctr=0
        
        while not rospy.is_shutdown():
            Trans = self.tf_b.lookup_transform(tcmd.header.frame_id,self.tip_link[0],rospy.Time(0),rospy.Duration(5.0))
            oc = np.array([Trans.transform.rotation.x,Trans.transform.rotation.y,Trans.transform.rotation.z,Trans.transform.rotation.w])
            
            Linear = Trans.transform.translation
            xc = np.array([Linear.x,Linear.y,Linear.z])
            trc = PyKDL.Vector(xc[0],xc[1],xc[2])
            rrc = PyKDL.Rotation.Quaternion(oc[0], oc[1], oc[2], oc[3])
            fc = PyKDL.Frame(rrc, trc)
            
            KDLtwist = PyKDL.diff(fc,fd,1.0)
            stp = max(abs(KDLtwist.vel.x()),abs(KDLtwist.vel.y()),abs(KDLtwist.vel.z()))
            sto = max(abs(KDLtwist.rot.x()),abs(KDLtwist.rot.y()),abs(KDLtwist.rot.z()))
            KDLtwist = self.saturateTwist(KDLtwist)
            (TR, TP, TY) = KDLtwist.rot

            rospy.loginfo(str(stp) + ' ' + str(sto))
            if ((stp <= 3e-3) and (sto <= 8e-3)):
                rospy.loginfo('Final Error Linear: %f m Angular:%f rads', stp, sto)
                rospy.loginfo('Position Stopping Commands')
                break
            if self.StopTriggered:
                rospy.loginfo('Final Error Linear: %f m Angular:%f rads', stp, sto)
                rospy.loginfo('Trigger Received Stopping Commands')
                break
            if (ctr >= max_cycles):
                rospy.loginfo('Final Error Linear: %f m Angular:%f rads', stp, sto)
                rospy.loginfo('Time out Stopping Commands')
                break

            tcmd.twist.linear.x = KDLtwist.vel.x() * Kp
            tcmd.twist.linear.y = KDLtwist.vel.y() * Kp
            tcmd.twist.linear.z = KDLtwist.vel.z() * Kp
            tcmd.twist.angular.x = TR * Kp 
            tcmd.twist.angular.y = TP * Kp 
            tcmd.twist.angular.z = TY * Kp
            tcmd.header.stamp = rospy.Time.now()
            ctr+=1
            self.pub.publish(tcmd)
            self.rate.sleep()
        Slx = tcmd.twist.linear.x 
        Sly = tcmd.twist.linear.y
        Slz = tcmd.twist.linear.z
        Sax = tcmd.twist.angular.x
        Say = tcmd.twist.angular.y
        Saz = tcmd.twist.angular.z
        t2 = 0.1;
        StCycles = int(0.2 * self.ctrl_rate)
        for i in range(StCycles):
            t = i/self.ctrl_rate
            # = 0
            #lx = 0
            #ly = 0
            #lz = 0
            tcmd.twist.linear.x = Slx - (2*Slx*t)/t2 + (Slx*(t**2))/t2
            tcmd.twist.linear.y = Sly - (2*Sly*t)/t2 + (Sly*(t**2))/t2
            tcmd.twist.linear.z = Slz - (2*Slz*t)/t2 + (Slz*(t**2))/t2
            tcmd.twist.angular.x = Sax - (2*Sax*t)/t2 + (Sax*(t**2))/t2 
            tcmd.twist.angular.y = Say - (2*Say*t)/t2 + (Say*(t**2))/t2 
            tcmd.twist.angular.z = Saz - (2*Saz*t)/t2 + (Saz*(t**2))/t2 
            tcmd.header.stamp = rospy.Time.now()
            self.pub.publish(tcmd)
            self.rate.sleep()
        resp = TaskPositionResponse()
        resp.success = True
        return resp
            

if __name__ == '__main__':
    rospy.init_node('Task_Mode_Handler', anonymous=True)
    MH = TaskModeHandle()
    rospy.Service('/lbr4/task_position_service', TaskPosition, MH.PositionModeServer)
    rospy.Service('/lbr4/stop_task_position_service', Trigger, MH.TriggerStopServer)
    rospy.spin()
