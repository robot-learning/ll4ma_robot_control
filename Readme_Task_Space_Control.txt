Launch the controller using lbr4_task_velocity_control.launch. This launches the task velocity control and the associated service for position tracking.
The following services are available:
/lbr4/task_position_service: Moves robot to desired position
/lbr4/stop_task_position_service: Stops the task_position_service to disengage the controller. Stops could be premptive.

Refer scripts/TaskPositionServiceDemo.py for an example of the client.

Note: The launch file is configured by default to work on the real robot with allegro. This will be extended later

TODO: Merge with README.md. Update simulation launch files to include task_space_control as an option.
